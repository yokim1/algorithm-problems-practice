﻿// IterativeInOrderTraversal.cpp : 이 파일에는 'main' 함수가 포함됩니다. 거기서 프로그램 실행이 시작되고 종료됩니다.
//

#include <iostream>
#include <vector>
#include <deque>
#include <cassert>

using namespace std;

class BinaryTree {
public:
    int value;
    BinaryTree* left;
    BinaryTree* right;
    BinaryTree* parent;

    BinaryTree(int value, BinaryTree* parent = NULL);
    void insert(vector<int> values, int i = 0);
};

BinaryTree::BinaryTree(int value, BinaryTree* parent) {
    this->value = value;
    this->left = NULL;
    this->right = NULL;
    this->parent = parent;
}

vector<int> testArray = {};

void testCallback(BinaryTree* tree) {
    if (tree == NULL) {
        return;
    }
    testArray.push_back(tree->value);
}

void iterativeInOrderTraversal(BinaryTree* tree,
    void (*callback)(BinaryTree* tree)) {
    tree->insert(0);
}


int main()
{
    BinaryTree* root = new BinaryTree(1, NULL);
    root->left = new BinaryTree(2, root);
    root->left->left = new BinaryTree(4, root->left);
    root->left->left->right = new BinaryTree(9, root->left->left);
    root->right = new BinaryTree(3, root);
    root->right->left = new BinaryTree(6, root->right);
    root->right->right = new BinaryTree(7, root->right);

    testArray = {};
    iterativeInOrderTraversal(root, &testCallback);
    vector<int> expected{ 4, 9, 2, 1, 6, 3, 7 };
    assert(testArray == expected);

    std::cout << "Hello World!\n";
}

// 프로그램 실행: <Ctrl+F5> 또는 [디버그] > [디버깅하지 않고 시작] 메뉴
// 프로그램 디버그: <F5> 키 또는 [디버그] > [디버깅 시작] 메뉴

// 시작을 위한 팁: 
//   1. [솔루션 탐색기] 창을 사용하여 파일을 추가/관리합니다.
//   2. [팀 탐색기] 창을 사용하여 소스 제어에 연결합니다.
//   3. [출력] 창을 사용하여 빌드 출력 및 기타 메시지를 확인합니다.
//   4. [오류 목록] 창을 사용하여 오류를 봅니다.
//   5. [프로젝트] > [새 항목 추가]로 이동하여 새 코드 파일을 만들거나, [프로젝트] > [기존 항목 추가]로 이동하여 기존 코드 파일을 프로젝트에 추가합니다.
//   6. 나중에 이 프로젝트를 다시 열려면 [파일] > [열기] > [프로젝트]로 이동하고 .sln 파일을 선택합니다.
