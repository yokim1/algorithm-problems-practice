﻿// MinHeightBST.cpp : 이 파일에는 'main' 함수가 포함됩니다. 거기서 프로그램 실행이 시작되고 종료됩니다.
//

#include <iostream>
#include <vector>
#include <cassert>

using namespace std;

class BST {
public:
    int value;
    BST* left;
    BST* right;

    BST(int value) {
        this->value = value;
        left = NULL;
        right = NULL;
    }

    void insert(int value) {
        if (value < this->value) {
            if (left == NULL) {
                left = new BST(value);
            }
            else {
                left->insert(value);
            }
        }
        else {
            if (right == NULL) {
                right = new BST(value);
            }
            else {
                right->insert(value);
            }
        }
    }
};

bool validateBstHelper(BST* tree, int minValue, int maxValue) {
    if (tree->value < minValue || tree->value >= maxValue) {
        return false;
    }
    if (tree->left != NULL &&
        !validateBstHelper(tree->left, minValue, tree->value)) {
        return false;
    }
    if (tree->right != NULL &&
        !validateBstHelper(tree->right, tree->value, maxValue)) {
        return false;
    }
    return true;
}

vector<int> inOrderTraverse(BST* tree, vector<int> array) {
    if (tree->left != NULL) {
        array = inOrderTraverse(tree->left, array);
    }
    array.push_back(tree->value);
    if (tree->right != NULL) {
        array = inOrderTraverse(tree->right, array);
    }
    return array;
}

bool validateBst(BST* tree) {
    return validateBstHelper(tree, INT_MIN, INT_MAX);
}

template <class BST> int getTreeHeight(BST* tree, int height) {
    if (tree == NULL)
        return height;
    int leftTreeHeight = getTreeHeight(tree->left, height + 1);
    int rightTreeHeight = getTreeHeight(tree->right, height + 1);
    return max(leftTreeHeight, rightTreeHeight);
}

BST* minHeightBstHelper(BST* bst, vector<int>array, int begin, int end);

BST* minHeightBst(vector<int> array) {
    
    int mid = array.size() / 2;
    int begin = 0;
    int end = array.size() - 1;

    BST* bst = NULL;

    if (array.size() == 1) {
        bst = new BST(array[0]);

        return bst;
    }
    else if (array.size() == 2) {
        bst = new BST(array[0]);
        bst->insert(array[1]);

        return bst;
    }

    return minHeightBstHelper(bst, array, begin, end);
}

BST* minHeightBstHelper(BST* bst, vector<int>array, int begin, int end) {

    int mid = begin + (abs(begin - end) / 2);

    if (bst == NULL) {
        bst = new BST(array[mid]);
    }
    else if (begin == end) {
        bst->insert(array[begin]);
        return bst;
    }
    else if (abs(begin - end) == 1) {
        bst->insert(array[begin]);
        bst->insert(array[end]);
        return bst;
    } else {
        bst->insert(array[mid]);
    }

    //left
    minHeightBstHelper(bst, array, begin, mid - 1);
    //right
    minHeightBstHelper(bst, array, mid + 1, end);

    return bst;
}

//             |                  halfSize = size() / 2
//   |                 |          0 ~ halfSize, halfSize + 1 ~ (size() - 1)
//0  1  2  3   4   5   6   7   8
//1, 2, 5, 7, 10, 13, 14, 15, 22



int main()
{
    //vector<int> array{ 1, 2, 5, 7, 10, 13, 14, 15, 22 };
    vector<int> array{1, 2, 5, 7};
    auto tree = minHeightBst(array);

    assert(validateBst(tree));
    assert(getTreeHeight(tree, 0) == 4);

    auto inOrder = inOrderTraverse(tree, {});
    vector<int> expected{ 1, 2, 5, 7, 10, 13, 14, 15, 22 };
    assert(inOrder == expected);

    std::cout << "Hello World!\n";
}

// 프로그램 실행: <Ctrl+F5> 또는 [디버그] > [디버깅하지 않고 시작] 메뉴
// 프로그램 디버그: <F5> 키 또는 [디버그] > [디버깅 시작] 메뉴

// 시작을 위한 팁: 
//   1. [솔루션 탐색기] 창을 사용하여 파일을 추가/관리합니다.
//   2. [팀 탐색기] 창을 사용하여 소스 제어에 연결합니다.
//   3. [출력] 창을 사용하여 빌드 출력 및 기타 메시지를 확인합니다.
//   4. [오류 목록] 창을 사용하여 오류를 봅니다.
//   5. [프로젝트] > [새 항목 추가]로 이동하여 새 코드 파일을 만들거나, [프로젝트] > [기존 항목 추가]로 이동하여 기존 코드 파일을 프로젝트에 추가합니다.
//   6. 나중에 이 프로젝트를 다시 열려면 [파일] > [열기] > [프로젝트]로 이동하고 .sln 파일을 선택합니다.
