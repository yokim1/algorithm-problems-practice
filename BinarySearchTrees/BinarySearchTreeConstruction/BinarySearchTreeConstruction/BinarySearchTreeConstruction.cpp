﻿// BinarySearchTreeConstruction.cpp : 이 파일에는 'main' 함수가 포함됩니다. 거기서 프로그램 실행이 시작되고 종료됩니다.
//

#include <iostream>
#include <vector>
#include <cassert>

using namespace std;

// Do not edit the class below except for
// the insert, contains, and remove methods.
// Feel free to add new properties and methods
// to the class.
class BST {
public:
    int value;
    BST* left;
    BST* right;

    BST(int val) {
        value = val;
        left = NULL;
        right = NULL;
    }

    BST& insert(int val) {
        if (val < value) {
            if (left == NULL) {
                BST* newNode = new BST(val);
                left = newNode;
            }
            else {
                left->insert(val);
            }
        }
        else if (value < val) {
            if (right == NULL) {
                BST* newNode = new BST(val);
                right = newNode;
            }
            else {
                right->insert(val);
            }
        }
        return *this;
    }

    bool contains(int val) {
        if (value == val) {
            return true;
        }
        else if (val < value) {
            if (left == NULL) {
                return false;
            }
            left->contains(val);
        }
        else if (value < val) {
            if (right == NULL) {
                return false;
            }
            right->contains(val);
        }
        else {
            return false;
        }
        return true;
    }
    
    BST* findMinValue() {
        if (left == NULL) {
            return this;
        }
        else {
            return left->findMinValue();
        }
    }

    BST& remove(int val) {

        if (value == val) {
            if (left != NULL && right != NULL) {//two child
                //find successor and replace this with it;
                BST* successor = right->findMinValue();
                value = successor->value;
                delete successor;
                successor = nullptr;
            }
            else if (left == NULL && right == NULL) {//no child
                *this = NULL;
            }
            else {//one child
                if (left != NULL) {
                    value = left->value;
                    left = left->left;
                    right = left->right;
                }
                else if (right != NULL) {
                    value = right->value;
                    left = right->left;
                    right = right->right;
                    
                }
            }
        }
        else {
            if (val < value) {
                left->remove(val);
            }
            else if (value < val) {
                right->remove(val);
            }
        }
        
        return *this;
    }
};

int main()
{
    BST* root = new BST(10);
    root->left = new BST(5);
    root->left->left = new BST(2);
    root->left->left->left = new BST(1);
    root->left->right = new BST(5);
    root->right = new BST(15);
    root->right->left = new BST(13);
    root->right->left->right = new BST(14);
    root->right->right = new BST(22);

    root->insert(12);
    assert(root->right->left->left->value == 12);

    root->remove(10);
    assert(root->contains(10) == false);
    assert(root->value == 12);

    assert(root->contains(15));
}

// 프로그램 실행: <Ctrl+F5> 또는 [디버그] > [디버깅하지 않고 시작] 메뉴
// 프로그램 디버그: <F5> 키 또는 [디버그] > [디버깅 시작] 메뉴

// 시작을 위한 팁: 
//   1. [솔루션 탐색기] 창을 사용하여 파일을 추가/관리합니다.
//   2. [팀 탐색기] 창을 사용하여 소스 제어에 연결합니다.
//   3. [출력] 창을 사용하여 빌드 출력 및 기타 메시지를 확인합니다.
//   4. [오류 목록] 창을 사용하여 오류를 봅니다.
//   5. [프로젝트] > [새 항목 추가]로 이동하여 새 코드 파일을 만들거나, [프로젝트] > [기존 항목 추가]로 이동하여 기존 코드 파일을 프로젝트에 추가합니다.
//   6. 나중에 이 프로젝트를 다시 열려면 [파일] > [열기] > [프로젝트]로 이동하고 .sln 파일을 선택합니다.
