﻿#include <iostream>
#include <vector>
#include <cassert>

using namespace std;

// This is an input class. Do not edit.
class BinaryTree {
public:
    int value;
    BinaryTree* left = nullptr;
    BinaryTree* right = nullptr;
    BinaryTree* parent = nullptr;

    BinaryTree(int value) { this->value = value; }
};

void InorderTraverse(BinaryTree* node, vector<BinaryTree*>& results);

BinaryTree* findSuccessor(BinaryTree* tree, BinaryTree* node) {

    vector<BinaryTree*> results;
    InorderTraverse(tree, results);
    results.push_back(nullptr);

    for (int i = 0; i < results.size(); ++i) {
        if (results[i] == node) {
            return results[i + 1];
        }
    }

    return nullptr;
}

void InorderTraverse(BinaryTree* node, vector<BinaryTree*>& results) {
    if (node != nullptr) {
        InorderTraverse(node->left, results);
        results.push_back(node);
        InorderTraverse(node->right, results);
    }
}

int main()
{
    BinaryTree* root = new BinaryTree(1);
    root->left = new BinaryTree(2);
    root->left->parent = root;
    root->right = new BinaryTree(3);
    root->right->parent = root;
    root->left->left = new BinaryTree(4);
    root->left->left->parent = root->left;
    root->left->right = new BinaryTree(5);
    root->left->right->parent = root->left;
    root->left->left->left = new BinaryTree(6);
    root->left->left->left->parent = root->left->left;
    auto node = root->left->right;
    auto expected = root;
    auto actual = findSuccessor(root, node);
    assert(expected == actual);


    return 0;
}