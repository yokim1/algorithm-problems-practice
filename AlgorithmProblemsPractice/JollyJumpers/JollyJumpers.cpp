﻿// JollyJumpers.cpp : 이 파일에는 'main' 함수가 포함됩니다. 거기서 프로그램 실행이 시작되고 종료됩니다.
//

//N개의 정수로 이루어진 수열에 대해 서로 인접해 있는 두 수의 차가 1에서 N - 1까지의 값을
//모두 가지면 그 수열을 유쾌한 점퍼(jolly jumper)라고 부른다.예를 들어 다음과 같은 수열에
//서 1 4 2 3 앞 뒤에 있는 숫자 차의 절대 값이 각각 3, 2, 1이므로 이 수열은 유쾌한 점퍼가
//된다.어떤 수열이 유쾌한 점퍼인지 판단할 수 있는 프로그램을 작성하라.
//▣ 입력설명
//첫 번째 줄에 자연수 N(3 <= N <= 100)이 주어진다.
//그 다음 줄에 N개의 정수가 주어진다.정수의 크기는 int 형 범위안에 있습니다.
//▣ 출력설명
//유쾌한 점퍼이면 “YES"를 출력하고, 그렇지 않으면 ”NO"를 출력한다.


#include <iostream>
using namespace std;

int numbers[100000];

int main()
{
    //freopen("input.txt", "rt", stdin);
    int n;
    int previous = NULL;
    int current;

    cin >> n;

    cin >> previous;
    for (int i = 2; i <= n; ++i) {
        cin >> current;

       // 1 ~ n - 1
        int pos = abs(previous - current);
       if (0 < pos && pos < n - 1 && numbers[pos] == 0) 
       {
          numbers[abs(previous - current)] = 1;
       }
       else 
       {
           cout << "NO" << endl;
           return 0;
       }

        previous = current;
    }
    cout << "YES" << endl;
    return 0;
}

// 프로그램 실행: <Ctrl+F5> 또는 [디버그] > [디버깅하지 않고 시작] 메뉴
// 프로그램 디버그: <F5> 키 또는 [디버그] > [디버깅 시작] 메뉴

// 시작을 위한 팁: 
//   1. [솔루션 탐색기] 창을 사용하여 파일을 추가/관리합니다.
//   2. [팀 탐색기] 창을 사용하여 소스 제어에 연결합니다.
//   3. [출력] 창을 사용하여 빌드 출력 및 기타 메시지를 확인합니다.
//   4. [오류 목록] 창을 사용하여 오류를 봅니다.
//   5. [프로젝트] > [새 항목 추가]로 이동하여 새 코드 파일을 만들거나, [프로젝트] > [기존 항목 추가]로 이동하여 기존 코드 파일을 프로젝트에 추가합니다.
//   6. 나중에 이 프로젝트를 다시 열려면 [파일] > [열기] > [프로젝트]로 이동하고 .sln 파일을 선택합니다.
