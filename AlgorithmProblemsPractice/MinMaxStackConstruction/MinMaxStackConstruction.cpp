﻿// MinMaxStackConstruction.cpp : 이 파일에는 'main' 함수가 포함됩니다. 거기서 프로그램 실행이 시작되고 종료됩니다.
//

#include <iostream>
#include <cassert>
#include <vector>

using namespace std;

// Feel free to add new properties and methods to the class.

class MinMaxStack {
private:
    vector<int> stack;
    vector<int> max;
    vector<int> min;

public:
    int peek() {
        return stack.back();
    }

    int pop() {
        int temp = stack.back();
        stack.pop_back();
        
        if (temp == max.back()) {
            max.pop_back();
        }
        
        if (temp == min.back()) {
            min.pop_back();
        }

        return temp;
    }

    void push(int number) {
        if (stack.empty()) {
            stack.push_back(number);
            max.push_back(number);
            min.push_back(number);
            return;
        }
        
        stack.push_back(number);

        if (max.back() <= number) {
            max.push_back(number);
        }
        
        if (number <= min.back()) {
            min.push_back(number);
        }
    }

    int getMin() {
        
        return min.back();
    }

    int getMax() {
        
        return max.back();
    }
};

int main()
{
    MinMaxStack stack;
    /*stack.push(5);
    stack.push(7);
    stack.push(2);
    stack.push(3);
    stack.push(1);
    stack.push(8);
    assert(stack.pop() == 8);
    assert(stack.pop() == 1);
    assert(stack.getMax() == 7);
    assert(stack.getMin() == 2);*/



    stack.push(5);
    stack.getMin();
    stack.getMax();
    stack.peek();
    stack.push(5);
    stack.getMin();
    stack.getMax();
    stack.peek();
    stack.push(5);
    stack.getMin();
    stack.getMax();
    stack.peek();
    stack.push(5);
    stack.getMin();
    stack.getMax();
    stack.peek();

}
