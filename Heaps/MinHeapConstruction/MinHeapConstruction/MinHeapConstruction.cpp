﻿// MinHeapConstruction.cpp : 이 파일에는 'main' 함수가 포함됩니다. 거기서 프로그램 실행이 시작되고 종료됩니다.
//

#include <iostream>
#include <vector>
#include <cassert>
using namespace std;

// Do not edit the class below except for the buildHeap,
// siftDown, siftUp, peek, remove, and insert methods.
// Feel free to add new properties and methods to the class.
class MinHeap {
public:
    vector<int> heap;

    MinHeap(vector<int> vector) {

        heap = buildHeap(vector); 
    
    }

    vector<int> buildHeap(vector<int>& vector) {

        heap = vector;

        return heap;
    }

    void swap(int& a, int& b) {
        int temp = a;
        a = b;
        b = temp;
    }

    void siftDown(int currentIdx, int endIdx, vector<int>& heap) {
        int childOneIdx = currentIdx * 2 + 1;

        while (childOneIdx <= endIdx) {
            int childTwoIdx = currentIdx * 2 + 2 <= endIdx ? currentIdx * 2 + 2 : -1;
            int idxToSwap;
            
        }
    }

    void siftUp(int currentIdx, vector<int>& heap) {
        if (heap[currentIdx] < heap[(currentIdx - 1) / 2] ) {
            swap(heap[currentIdx], heap[(currentIdx - 1) / 2]);
            siftUp((currentIdx-1)/2, heap);
        }
        else {
            return;
        }
    }

    int peek() {

        return heap.front();
    }

    int remove() {
        swap(heap[0], heap[heap.size() - 1]);
        int temp = heap.back();
        heap.pop_back();

        siftDown(0, heap.size() - 1, heap);

        return temp;
    }

    void insert(int value) {
        heap.push_back(value);
        siftUp(heap.size()-1, heap);
    }
};

bool isMinHeapPropertySatisfied(vector<int> array) {
    for (int currentIdx = 1; currentIdx < array.size(); currentIdx++) {
        int parentIdx = (currentIdx - 1) / 2;
        if (parentIdx < 0) {
            return true;
        }
        if (array[parentIdx] > array[currentIdx]) {
            return false;
        }
    }
    return true;
}

int main()
{
    MinHeap minHeap({ 48, 12, 24, 7, 8, -5, 24, 391, 24, 56, 2, 6, 8, 41 });
    minHeap.insert(76);
    assert(isMinHeapPropertySatisfied(minHeap.heap));
    assert(minHeap.peek() == -5);
    assert(minHeap.remove() == -5);
    assert(isMinHeapPropertySatisfied(minHeap.heap));
    assert(minHeap.peek() == 2);
    assert(minHeap.remove() == 2);
    assert(isMinHeapPropertySatisfied(minHeap.heap));
    assert(minHeap.peek() == 6);
    minHeap.insert(87);
    assert(isMinHeapPropertySatisfied(minHeap.heap));
}